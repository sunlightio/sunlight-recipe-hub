#!/bin/bash

sudo ./prep_vm2.sh

ncpus=`grep processor /proc/cpuinfo | awk '{print $3}' | wc -l`

while true; do
    sudo ./fio-3.2 ./randread$ncpus.fio

    sleep 2
done
