#!/bin/sh

fix_irqbalance () {
	irqs=$1
	banstr=''

	for irq in $irqs; do
		banstr="$banstr --banirq=$irq"
	done

	echo $banstr
}

ncpus=`grep processor /proc/cpuinfo | awk '{print $3}' | wc -l`

irqs=`grep "blkif" /proc/interrupts  | awk '{print $1}' | tr -d ':'`

pgrep irqbalance &> /dev/null
if [ $? -eq 0 ]; then
	irq_balance_args=`fix_irqbalance "$irqs"`

	killall -9 irqbalance

	irqbalance $irq_balance_args
fi

cpu=0
for irq in $irqs; do
	cpu=$(( $cpu % $ncpus ))
	mask=`printf "%x" $(( 1 << $cpu ))`
	echo $mask > /proc/irq/$irq/smp_affinity
	cpu=$(( $cpu + 1 ))
done
