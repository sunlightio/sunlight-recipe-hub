
- name: Retrieve the IP address of the primary interface of instances
  hosts: heavyforwarders
  gather_facts: False
  remote_user: root

  tasks:
    - name: Check if the instance info file exists.
      local_action: stat path=/root/recipe_manager/deployment/{{ deployment_id }}/{{name | regex_replace(\" \", \"_\")}}.instance
      register: instance_stat

    - name: End the play for the instance which wasn't created under this recipe
      meta: end_host
      when: not instance_stat.stat.exists

    - name: Read the instance info from file
      set_fact:
        instance_info: "{{ lookup('file', '/root/recipe_manager/deployment/{{ deployment_id }}/{{name | regex_replace(\" \", \"_\")}}.instance') }}"

    - set_fact:
        all_network_ids: "{{ instance_info | json_query('networks[*].network_id') }}"
        all_interface_ips: "{{ instance_info | json_query('networks[*].ip') }}"

    - name: Read the Splunk indexers' info from the dependent deployment info file
      set_fact:
        indexer_1_info: "{{ lookup('file', '/root/recipe_manager/deployment/{{ dependent_deployment_id }}/Indexer_1.instance') }}"
        indexer_2_info: "{{ lookup('file', '/root/recipe_manager/deployment/{{ dependent_deployment_id }}/Indexer_2.instance') }}"
        indexer_3_info: "{{ lookup('file', '/root/recipe_manager/deployment/{{ dependent_deployment_id }}/Indexer_3.instance') }}"

    - set_fact:
        indexer_1_all_network_ids: "{{ indexer_1_info | json_query('networks[*].network_id') }}"
        indexer_1_all_interface_ips: "{{ indexer_1_info | json_query('networks[*].ip') }}"
        indexer_2_all_network_ids: "{{ indexer_2_info | json_query('networks[*].network_id') }}"
        indexer_2_all_interface_ips: "{{ indexer_2_info | json_query('networks[*].ip') }}"
        indexer_3_all_network_ids: "{{ indexer_3_info | json_query('networks[*].network_id') }}"
        indexer_3_all_interface_ips: "{{ indexer_3_info | json_query('networks[*].ip') }}"

    - name: Query the network IDs
      delegate_to: sunlight_controller
      command: osd get_networks --json --filter=name,id
      register: networks_list_return

    - name: Get the network id by its name
      set_fact:
        network_id_by_name: "{{ dict (networks_list_return.stdout | from_json | json_query('data.names') | zip (networks_list_return.stdout | from_json | json_query('data.ids'))) }}"

    - set_fact:
        interface_ip_by_id: "{{ dict( all_network_ids | zip(all_interface_ips) ) }}"

    - name: Get the network name of the instance
      set_fact:
        primary_net_id: "{{ network_id_by_name[network.split(',')[0]] }}"

    - name: Get IP address of the primary interface on client instance
      set_fact:
        primary_interface_ip: "{{ interface_ip_by_id[primary_net_id | int] }}"

    - debug:
        msg: "instance {{ name }} has IP: {{ primary_interface_ip}}"

    - name: find the interface of Index1 which is in the same network as the HF instance
      set_fact:
        indexer_1_interface_ip: "{{ indexer_1_all_interface_ips[index] }}"
      when: item|int == primary_net_id|int
      loop: "{{ indexer_1_all_network_ids }}"
      loop_control:
        index_var: index

    - name: find the interface of Index2 which is in the same network as the HF instance
      set_fact:
        indexer_2_interface_ip: "{{ indexer_2_all_interface_ips[index] }}"
      when: item|int == primary_net_id|int
      loop: "{{ indexer_2_all_network_ids }}"
      loop_control:
        index_var: index

    - name: find the interface of Index3 which is in the same network as the HF instance
      set_fact:
        indexer_3_interface_ip: "{{ indexer_3_all_interface_ips[index] }}"
      when: item|int == primary_net_id|int
      loop: "{{ indexer_3_all_network_ids }}"
      loop_control:
        index_var: index

- name: Deploy & Configure Splunk Heavy Forwarders
  hosts: heavyforwarders
  gather_facts: False
  remote_user: root

  tasks:
    - name: Wait until the instance is up and reachable
      delegate_to: sunlight_controller
      shell: ping -c 1 -W 1 {{primary_interface_ip}}
      register: reachability
      until: reachability.rc == 0
      delay: 2
      retries: 300

    - name: Wait 90 seconds for the splunk instance to become reachable/usable
      delegate_to: sunlight_controller
      command: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@{{ primary_interface_ip }} exit
      register: reachability
      until: reachability.rc == 0
      delay: 2
      retries: 90

    - name: Copy the Apps to the Sunlight controller
      delegate_to: sunlight_controller
      copy:
        src: splunk_configs_hf
        dest: /tmp/
        mode: u=rwx,g=rx,o=rx

    - name: Unzip the Apps on the controller
      delegate_to: sunlight_controller
      command: tar -xzf  /tmp/splunk_configs_hf/apps_hf.tar.gz -C /tmp/splunk_configs_hf/

    - name: Update the indexer IP address for sunlight_cluster_forwarder_outputs
      delegate_to: sunlight_controller
      lineinfile:
        path: /tmp/splunk_configs_hf/apps/sunlight_cluster_forwarder_outputs/local/outputs.conf
        regexp: '^server ='
        line: "server = {{ indexer_1_interface_ip }}:9997,{{ indexer_2_interface_ip }}:9997,{{ indexer_3_interface_ip }}:9997"

    - name: Update the indexer IP address for sunlight_datagen_forwarder_outputs
      delegate_to: sunlight_controller
      lineinfile:
        path: /tmp/splunk_configs_hf/apps/sunlight_datagen_forwarder_outputs/local/outputs.conf
        regexp: '^server ='
        line: "server = {{ indexer_1_interface_ip }}:9997,{{ indexer_2_interface_ip }}:9997,{{ indexer_3_interface_ip }}:9997"

    - name: Update the indexer IP address for collectd script
      delegate_to: sunlight_controller
      lineinfile:
        path: /tmp/splunk_configs_hf/apps/sunlight_collectd_notSplunk/collectd.conf
        regexp: '^URL "https'
        line: "URL \"https://{{ indexer_1_interface_ip }}:8088/services/collector/raw\""

    - name: Update the management node IP address for sunlight_full_license_server
      delegate_to: sunlight_controller
      lineinfile:
        path: /tmp/splunk_configs_hf/apps/sunlight_full_license_server/local/server.conf
        regexp: '^master_uri ='
        line: "master_uri = https://{{ hostvars['mgnt']['primary_interface_ip'] }}:8089"

#    - name: Update the indexer IP address for eventgen in sample_data
#      delegate_to: sunlight_controller
#      lineinfile:
#        path: /tmp/splunk_configs_c1/apps/sample_data/local/eventgen.conf
#        regexp: '^splunkHost ='
#        line: "splunkHost = {{ hostvars['indexer1']['primary_interface_ip'] }}"

    - name: Add user seed to all
      delegate_to: sunlight_controller
      command: scp -r -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r {{ item }}
      loop:
          - "/tmp/splunk_configs_hf/user-seed.conf ubuntu@{{ primary_interface_ip }}:"

    - name: Add users' pub key to all
      delegate_to: sunlight_controller
      command: scp -r -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r {{ item }}
      loop:
          - "/tmp/splunk_configs_hf/users.pub ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/sunlight_collectd_notSplunk ubuntu@{{ primary_interface_ip }}:"

    # other splunk version list: https://www.splunk.com/en_us/download/sem.html
    - name: Install the splunk on all instances
      delegate_to: sunlight_controller
      command: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@{{ primary_interface_ip }} "{{ item }}"
      loop:
        - sudo bash -c 'echo nameserver 8.8.8.8 >> /etc/resolv.conf';
        - cat users.pub | tee -a ~/.ssh/authorized_keys
          #- sudo ip route delete default dev eth1;
        - wget -O splunk.deb 'https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=8.1.2&product=splunk&filename=splunk-8.1.2-545206cc9f70-linux-2.6-amd64.deb&wget=true'
        - sudo dpkg -i splunk.deb
        - sudo mv user-seed.conf /opt/splunk/etc/system/local/
        - echo "sunlight123" | sudo tee /opt/splunk/etc/auth/splunk.secret
        - sudo chmod 0400 /opt/splunk/etc/auth/splunk.secret
        - sudo apt-get update -y
        - sudo DEBIAN_FRONTEND=noninteractive apt-get -y install collectd
        - sudo mv sunlight_collectd_notSplunk/collectd.conf /etc/collectd/collectd.conf
        - sudo systemctl restart collectd.service

    - name: Add configuration for heavy forwarder
      block:
      - name: Copy configuration to heavy forwarder VM
        delegate_to: sunlight_controller
        command: scp -r -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r {{ item }}
        loop:
          - "/tmp/splunk_configs_hf/apps/sunlight_full_license_server ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/sunlight_datagen_forwarder_outputs ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/SA-Eventgen ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/sample_data ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/Splunk_TA_bluecoat-proxysg ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/Splunk_TA_mcafee ubuntu@{{ primary_interface_ip }}:"
          - "/tmp/splunk_configs_hf/apps/Splunk_TA_sophos ubuntu@{{ primary_interface_ip }}:"

      - name: Apply Configuration to heavy Forwarder VM
        delegate_to: sunlight_controller
        command: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@{{ primary_interface_ip }} "{{ item }}"
        with_items:
          - sudo mv sunlight_full_license_server /opt/splunk/etc/apps/sunlight_full_license_server
          - sudo mv sunlight_datagen_forwarder_outputs /opt/splunk/etc/apps/sunlight_datagen_forwarder_outputs
          - sudo mv SA-Eventgen /opt/splunk/etc/apps/SA-Eventgen
          - sudo mv sample_data /opt/splunk/etc/apps/sample_data
          - sudo mv Splunk_TA_bluecoat-proxysg /opt/splunk/etc/apps/Splunk_TA_bluecoat-proxysg
          - sudo mv Splunk_TA_mcafee /opt/splunk/etc/apps/Splunk_TA_mcafee
          - sudo mv Splunk_TA_sophos /opt/splunk/etc/apps/Splunk_TA_sophos
      when: splunk_tier == 'collection'

    - name: Start Splunk on all instances
      delegate_to: sunlight_controller
      command: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@{{ primary_interface_ip }} "{{ item }}"
      loop:
        - sudo /opt/splunk/bin/splunk start --accept-license --no-prompt

