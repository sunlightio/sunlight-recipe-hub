#!/bin/sh

fix_irqbalance () {
        irqs=$1
        banstr=''

        for irq in $irqs; do
                banstr="$banstr --banirq=$irq"
        done

        echo $banstr
}

ncpus=`grep processor /proc/cpuinfo | awk '{print $3}' | wc -l`

irqs=`grep "blkif" /proc/interrupts  | awk '{print $1}' | tr -d ':'`

pgrep irqbalance &> /dev/null
if [ $? -eq 0 ]; then
        echo Fixing irqbalance
        irq_balance_args=`fix_irqbalance "$irqs"`

        killall -9 irqbalance

        out=`irqbalance $irq_balance_args`
fi

echo Setting up irqs
cpu=0
for irq in $irqs; do
        cpu=$(( $cpu % $ncpus ))
        parts=$(( $ncpus / 32 ))

        active_part=$(( $cpu / 32 ))

        final_mask=""
        for i in `seq 1 $active_part`; do
                final_mask=,00000000$final_mask
        done

        new_cpu=$(( $cpu % 32 ))
        mask=`printf "%x" $(( 1 << $new_cpu ))`
        final_mask=$mask$final_mask
        echo $final_mask > /proc/irq/$irq/smp_affinity
        cpu=$(( $cpu + 1 ))
done

echo All done.
