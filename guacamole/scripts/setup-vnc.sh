sudo apt update
echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections
sudo apt install xfce4 xfce4-goodies -y
sudo apt install tightvncserver -y
mkdir ~/.vnc
cat <<EOT >> ~/.vnc/xstartup
#!/bin/bash
xrdb $HOME/.Xresources
startxfce4 &
EOT
chmod a+x ~/.vnc/xstartup
vncpasswd="$1"
echo $vncpasswd | vncpasswd -f > ~/.vnc/passwd
chmod 0600 ~/.vnc/passwd
vncserver
