# Sunlight Recipe Hub #

The Sunlight Recipe Hub contains the most popular applications and commonly-used tools to be deployed on the Sunlight platform. The recipes could be used by adding the Sunlight Recipe Hub into the Sunlight Recipe Manager.

If you haven't deployed the Sunlight Recipe Manager, you can find more information about it at [its git repository](https://bitbucket.org/sunlightio/recipe_manager). 

# Guide of Using the Recipes

## benchmark-tools

The benchmark-tools recipe can deploy the benchmarking tools including Fio and VDBench to the instances for the purpose of measuring the performance in the instances.

The number of instances and their flavors are selected by Recipe Manager automatically according to the avaialble resources of the cluster to deploy on.

## fio-workload

The fio-workload recipe can kick off the storage I/O workload in the instances for the purpose of demonstrating the storage performance.

The number of instances and their flavors are selected by Recipe Manager automatically according to the avaialble resources of the cluster to deploy on.

## cloudera

The cloudera recipe can deploy the CDP (Cloudera Data Platform) Private Cloud Base to multiple designated instances.

The instances as Cloudera nodes are specified in the execution environment descriptor file, i.e. cloudera.yml. You can edit this descriptor file to change the flavor of instance and number of instances. This recipe is created for the instances running RHEL/CentOS compatible operating systems.

The following configurations in the descriptor file are specific to the cloudera recipe and they are required to be updated when necessary.

The instance which serves as Cloudera Manager needs to be configured as *master* node. You can change the Cloudera Manager version by changing the *cloudera_repo* and *cloudera_repo_key*. In addition, the login and password of the license used to download the Cloudera installation packages are required to be provided.

```
vm1:
  name: cloudera manager server
  cloudera_node_type: master
  cloudera_repo: archive.cloudera.com/p/cm7/7.1.3/redhat7/yum/cloudera-manager.repo
  cloudera_repo_key: archive.cloudera.com/p/cm7/7.1.3/redhat7/yum/RPM-GPG-KEY-cloudera
  cloudera_license_login: changeme
  cloudera_license_password: changeme
```

The instance which serves as worker node of Cloudera cluster needs to be configured as *worker* node.

```
vm2:
  name: cloudera worker 1
  cloudera_node_type: worker
```

For more information about deployment of Cloudera, you can find at the official guide provided by Cloudera.

Reference: https://docs.cloudera.com/cdp-private-cloud-base/7.1.3/installation/topics/cdpdc-prod-installation.html

## guacamole

The Apache Guacamole is a clientless remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH.

### guaca-server

The guaca-server recipe can deploy Guacamole server as a desktop broker.

You can configure the networks attached to the Guacamole server in the execution environment descriptor file, i.e. guaca-server.yml.

### guaca-desktop

The guaca-desktop recipe can deploy the remote desktops and connect them to the Guacamole server. The guaca-desktop recipe is dependent on guaca-server recipe.

You can configure the flavor and image of the instances running as remote desktops in the execution evironment descriptor file, i.e. guaca-desktop.yml

The following configurations in the descriptor file are specific to the guaca-desktop recipe and they are required to be configured in accordance to the remote desktop protocol used for the instance.

```
  guacamole_connection_protocol: ssh
  guacamole_connection_protocol: vnc
  guacamole_connection_protocol: rdp
```

Reference: https://guacamole.apache.org/

## kubernetes

The k8s recipe can deploy a Kubernetes cluster on the Sunlight platform.

## sl-template-server

The sl-template-server recipe can deploy a local Sunlight template server on the Sunlight platform to allow you to upload your own image to this template server and add this template server as an additional image repository.

When this recipe is deployed, you can follow [this instruction](http://docs.sunlight.io/local_template_server/) provided by Sunlight to import your own image as a template.

Reference: http://docs.sunlight.io/local_template_server/

## sl-update

The sl-update recipe can check the version of the cluster and update it to the latest if required.

## splunk

The splunk recipe can deploy Splunk on the Sunlight platform.

Reference: https://www.splunk.com/en_us/download/sem.html
